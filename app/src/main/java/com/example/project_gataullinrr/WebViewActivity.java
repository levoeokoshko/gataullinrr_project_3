package com.example.project_gataullinrr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends AppCompatActivity {

    String login, website;
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Intent loginIntent = getIntent();
        login = loginIntent.getStringExtra("login");
        website = loginIntent.getStringExtra("website");

        setTitle(getTitle()+" | Cool username: "+login);
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(website);
    }


    // Процедура, позволяющая вернуться на предыдущую веб-страницу
    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
        {
            webView.goBack();
        }
        else
            super.onBackPressed();
    }
}
