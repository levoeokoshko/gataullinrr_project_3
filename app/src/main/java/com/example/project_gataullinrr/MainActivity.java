package com.example.project_gataullinrr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent; 
import android.view.View;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/* Это новый комментарий, выполненный в новой ветке

___________$$$$$$
___________$$_____$$
__________$__(•)____$$    Кря
________$$__________$
___________$$_____$
___________$____$
_________$____$__$$$__$$______$
_______$$_____$_____$$__$$__$$$
_______$______$___________$$__$
_______$$_______$______$$_____$
_______$$________$$$$$$______$
________$$$________________$
__________$$$$__________$$
____________$$$$$$$$$$$$

*/


public class MainActivity extends AppCompatActivity {

    TextView errorOut;
    EditText passwordEdit, loginEdit;
    Context ctx;
    String[][] userdata = new String[20][3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;
        String[] logData = getResources().getStringArray(R.array.nodes);

        for(int i = 0; i < logData.length; i++) {
            userdata[i] = logData[i].split("-");
        }

        errorOut = findViewById(R.id.errorOutput);
        loginEdit = findViewById(R.id.login);
        passwordEdit = findViewById(R.id.password);
    }

    public void onMyButtonClick(View view){
        //Log.i("MyApplication", "click on my button");

        String passwordValue = passwordEdit.getText().toString();
        String loginValue = loginEdit.getText().toString();

        //Log.i("Login", loginValue);
        //Log.i("Password", passwordValue);


        boolean flag = true;
        int foundIndex = -1;
        for (int i = 0; (i < userdata.length) && flag; i++)
        {
            if (userdata[i][0] != null)
            {
                if (userdata[i][0].equals(loginValue) && userdata[i][1].equals(passwordValue))
                {
                    flag = false;
                    foundIndex = i;
                }
            }
            else {
                flag = false;
            }
        }

        if (foundIndex >= 0) {
            Toast.makeText(getApplicationContext(), "Successful authorization!", Toast.LENGTH_SHORT).show();

            // Объявляем Intent
            Intent loginIntent = new Intent(ctx, WebViewActivity.class);

            //Добавляем туда значения логина и сайта
            loginIntent.putExtra("login", loginValue);
            loginIntent.putExtra("website", userdata[foundIndex][2]);

            //Запускаем активити
            startActivity(loginIntent);
        }
        else {
            Toast.makeText(getApplicationContext(), "Wrong Login or Password!", Toast.LENGTH_SHORT).show();
        }

    }

}
